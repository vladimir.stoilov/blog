---
title: Combinatorics
date: "2019-05-27"
description: Breaf explanation of Permutation and Combination.
---

Combinatorics is used almost in all parts of mathematics. In software development is used for developing and analysing algorithms.

## Permutation

In mathematics permutation is an act of ordering the elements of a set into a sequence or order.

The number of permutations of $n$ distinct objects is $n$ [factorial](https://en.wikipedia.org/wiki/Factorial) written as $n!$, which means the product all positive integers less then or equal to $n$.

$$
P_{n} = n!
$$

##### Example: 

You want to order 10 distinct numbered balls. How many arrangements you can come up with? 

##### Solution:  

$n =  10$  

$$P_{10} = 10! = 3628800$$

### k-permutations of n

##### or "variations without repetition" usually used by non-english authors.

The number of ways we can choose and order a subset of $k$ elements from a set of $n$ elements.  

General equation:  

$$
P^{n}_{k} = \underbrace{n \times (n - 1) \times ... \times (n - k + 1)}_{k factors}
$$

And here is a simplified version:  

$$
P^{n}_{k} =\begin{cases}
0 & k > n \\
\frac{n!}{(n - k)!} & k \leqq n
\end{cases}
$$

##### Example:

You want to put 5 different keys in 10 different boxes. In how many ways you can do that?  

##### Solution:  

$n = 10, k = 5$
$$
P^{10}_5 = \frac{10!}{(10 - 5)!} = \frac{10!}{5!} = 10 \times 9 \times 8 \times 7 \times 6 = 30240
$$

Permutation is a special case of k-permutation of n. If $k = n$ then we end up with:  

$$
\frac{n!}{(n-k)!} = \frac{n!}{0!}
$$

Because $0! = 1$ we end up with $n!$ which is $P_n$.

### k-permutation with repetition

##### or "variations with repetition" usually used by non-english authors.

If you understood k-permutation of n the name of this part is self explanatory. It has the same concept but we can choose one element more then once.

$$V^k_n = n^k$$  

##### Example:

You want to choose a 4 digit PIN number for your phone's lock screen. How many PIN combinations are there?  

0 1 2 3 4 5 6 7 8 9. 

##### Solution:

$n = 10, k = 4$
$$
V^{10}_{4} = 10^4 = 10000
$$

## Combination

The difference between permutation and combination is that combination dose not care about the order of the elements. We care only which elements we pick. If the elements we want to pick $k$ is equal to the elements we can pick from $n$. If $k = n$ then the number of combinations is 1. 

### Number of combinations

It is usually denoted with $C^n_k$ or with a **binomial coefficient** $\binom{n}{k}$

General equation:  

$$
\binom{n}{k} = \frac{n \times (n - 1) \times ... \times(n - k + 1) }{k \times (k - 1) \times...\times1}
$$

simplified version:  

$$
\binom{n}{k} = \begin{cases}
0 & k > n \\
1 & k = n \\
\frac{n!}{k! \times (n-k)!} & k < n
\end{cases}
$$

##### Example: 

You want to choose 5 boys for a basketball team from a group of 10 boys. In how many ways you can do that?

##### Solution:

$n = 10, k = 5$

$$
\binom{10}{5} = \frac{10!}{5! \times (10 - 5)!} = \frac{10!}{5! \times 5!} = 252
$$

### Number of combinations with repetition

Again the order is not taken into account and we can choose one element multiple times. it is denoted $\overline{C^k_n}$ or $\big(\!\!\binom{n}{k}\!\!\big)$.

It can be represented with combination:  
$$
\overline{C^k_n} = C^k_{n + k - 1}
$$
Because it's a it can be represented with combination the equations are veary simimilar. We have the same denominator and just a few changes to the numerator.  
$$
\bigg(\!\!\binom{n}{k}\!\!\bigg) = \frac{(n + k - 1) \times (n + k - 2) \times ... \times n}{k \times (k - 1) \times ... \times 1}
$$

simplified version:  
$$
\bigg(\!\!\binom{n}{k}\!\!\bigg) = \binom{n + k - 1}{k} = \frac{(n + k - 1)!}{k! \times (n - 1)!}
$$

##### Example:

You have 10 unsorted arrays and 5 sorting algorithms. In how many ways you can sort all the arrays with the given algorithms?

##### Solution: 

$$
\bigg(\!\!\binom{5}{10}\!\!\bigg) = \frac{(5 + 10 -1)!}{10! \times (5 - 1)!} =\frac{14!}{10! \times 4!} = 1001
$$

---

### [Ferder reading](https://en.wikipedia.org/wiki/Combinatorics)

---

##  Source:
* [Wikipedia Combinatorics](https://en.wikipedia.org/wiki/Combinatorics)
* Discrete mathematics - С. Бойчева