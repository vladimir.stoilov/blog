---
title: Pandoc lua filters Tutorial
description: Tutorial how to write your own lua filters in pandoc.
date: "2019-06-13"
---

First you need `pandoc 2` to run `lua` code. You can get it from [pandoc's website](https://pandoc.org/installing.html). 

## Prerender Math

Starting with a very simple filter we will convert latex math to html renderable math.

The goal is to convert  
`$\hat{f}\xi\,e^{2 \pi i \xi x}\,d\xi$` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$\hat{f}\xi\,e^{2 \pi i \xi x}\,d\xi$  
and 

```latex
$$
z \left( 1 \ +\ \sqrt{\omega_{i+1} + \zeta -\frac{x+1}{\Theta +1} y + 1}
\ \right)
\ \ \ =\ \ \ 1
$$
```
to
$$
z \left( 1 \ +\ \sqrt{\omega_{i+1} + \zeta -\frac{x+1}{\Theta +1} y + 1}
\ \right)
\ \ \ =\ \ \ 1
$$

#### The solution:
```lua
math_blocks = {
   Math = function(el)      
      if el.mathtype == 'InlineMath' then
        local html = pandoc.pipe('katex', {}, el.text) -- run katex with math as input
        return pandoc.RawInline('html', html)          -- return plain html
      elseif el.mathtype == 'DisplayMath' then
        local html = pandoc.pipe('katex', {'-d'}, el.text) -- run katex with -d argument
        return pandoc.RawInline('html', html)              -- return plain html
      end
   end
}

function Pandoc(el)
   local div = pandoc.Div(el.blocks)
   local pan = pandoc.walk_block(div, math_blocks).content
   return pandoc.Pandoc(pan, el.meta)
end

```
When you run this command the Lua script will be executed ("katex.lua" is the file containing the lua code)

`pandoc --lua-filter katex.lua input.md -o output.html`

**This filter works only with html output.**  
Im using [Katex CLI](https://katex.org/docs/cli.html) to pre render the math to html. This is better when you have static math in the page. 

`pandoc.walk_block` function walks through every block in the markdown file and if it finds a block that is defined in  `math_blocks` it calls the defined function for the specific block. 

`el` is the math block element in this case.

We have two possibilities 'InlineMath' witch is inlined math and 'DisplayMath' which is math on its own section.

With `pandoc.pipe('katex', {}, el.text)` we execute command line program with no arguments and the latex math as pipe input (which is plain text). We have to call it with `-d` for equations that needs its own section.

You can read more about how to use pandoc.pipe in [pandoc pipe documentation](https://pandoc.org/lua-filters.html#pipe)

And when we return `pandoc.RawInline('html', html)`, pandoc replaces the math element with the returned value. In this case is raw html straight from katex output.

This is just scratching the surface what pandoc filters can do. Here is some link you can learn more about how to write your own:
* [Pandoc lua documentation](https://pandoc.org/lua-filters.html)
* [Github pandoc lua-filters](https://github.com/pandoc/lua-filters)
