---
title: Sorting Algorithms
date: "2019-05-10"
description: Insertion sort, Merge sort, Quick sort.
---

The sorting problem is reordering sequence of numbers such as:  
Input: sequence of n numbers $(a_{1} ,a_{2}, ... , a_{n})$  
Output: sequence of the same numbers $(a^{'}_{1}, a_{2}^{'},..., a_{n}^{'})$ reordered such as 
$a_{1}^{'} \le a_{2}^{'} \le ... \le a_{n}^{'}$.  
The sorting algorithms solve that problem. They are many sorting algorithms but in this blog post we will look at a three pretty popular algorithms.  

  - [**Insertion Sort**](#insertion-sort)  
  - [**Merge Sort**](#merge-sort)  
  - [**Quick Sort**](#quick-sort)  

I think the best way to understand algorithm is to look at the code. Even if you are beginner programmer you should first try to understand the code. I have put a brief explanation of how the algorithms works and links to further reading.  

## **Insertion Sort**
Insertion Sort has advantages that is very easy to implement, it's fast on small input sizes, it's stable (keeps the order of the elements with equal value) and very efficient on data sets that are nearly sorted.  

- Worst case performance: $O(n^{2})$  
- Average case performance: $O(n^{2})$  
- Best case performance: $O(n)$  

![](insertion_sort.jpg)

```c
void sort( int* numbers, int n ) {
    for( int j = 1; j < n; j++ ) {
        int key = numbers[j];
        int i = j - 1;
        while( i >= 0 && numbers[i] > key ) {
            numbers[i + 1] = numbers[i];
            i -= 1;
        }
        numbers[i + 1] = key;
    }
}
```
The code is pretty simple and self explanatory.
 1. Select the second element.
 2. Compare with the previos element.
 3. Check if the element left from it is grater.
    * If grater switch places and repeat p. 3
    * If smaller continue.
 4. Increase counter and go to p.2.  


Further reading: [Wikipedia Merge Sort](https://en.wikipedia.org/wiki/Insertion_sort).  

## **Merge Sort** 

Merge sort is an algorithm that uses [divide-and-conquer](https://en.wikipedia.org/wiki/Divide-and-conquer_algorithm) approach, in most implementations it is stable and gives the same time for all data sets with the same size no matter how they are ordered. It is more efficient then Insertion sort on large input sizes and was invented by [John von Neumann](https://en.wikipedia.org/wiki/John_von_Neumann) (a mathematical genius).  

-  Worst case performance: $O(n\ln(n))$  
-  Average case performance: $O(n\ln(n))$  
-  Best case performance: $O(n\ln(n))$  


![](merge_sort.jpg)

```c
#include <limits.h>

void merge( int* numbers, int beginning, int middle, int end ) {
    int size1 = middle - beginning + 1;
    int size2 = end - middle;
    int array1[size1 + 1];
    int array2[size2 + 1];

    for( int i = 0; i < size1; i++ )
        array1[i] = numbers[beginning + i];

    for( int i = 0; i < size2; i++ )
        array2[i] = numbers[middle + i + 1];

    array1[size1] = INT_MAX;
    array2[size2] = INT_MAX;

    int i = 0;
    int j = 0;
    for( int k = beginning; k <= end; k++ ) {
        if( array1[i] <= array2[j] ) {
            numbers[k] = array1[i];
            i++;
        } else {
            numbers[k] = array2[j];
            j++;
        }
    }
} 

void mergeSort( int* numbers, int beginning, int end ) {
    if( beginning < end ) {
        int middle = ( beginning + end ) / 2;
        mergeSort( numbers, beginning, middle );
        mergeSort( numbers, middle + 1, end );
        merge( numbers, beginning, middle, end );
    }
}

```

It starts by recursively splitting the input array until it ends with arrays with size `1`. Then it performers `merge` on every array pair which comes form the same array. The merging begins with comparing the elements form both arrays and placing them one by one in the output array. When we merge all the elements we end up with one sorted array.

The implementation that I provided is just for understanding how Merge sort works. It lacks optimization and it works only with integer smaller then `INT_MAX` wich is placed so we dont have to check for the end of the array.  

Further reading: [Wikipedia Merge Sort](https://en.wikipedia.org/wiki/Merge_sort).

## **Quick Sort**

Quick sort is the most command sorting algorithm, it’s implemented in almost all standard libraries. In the worst case quick sort is actually slower then Merge sort and Heap sort but the reason why is more common is that the average case is faster.
 Quick sort is **not stable** and it's more complicated to implement. It was invented by [Tony Hoare](https://en.wikipedia.org/wiki/Tony_Hoare).  

- Worst case performance: $O(n^{2})$  
- Average case performance: $O(nln(n))$  
- Best case performance: $O(n)$  

![](quick_sort.jpg)

-- Note: the image is not a direct corolation between worst, average and best case. The image is generated with counting the iterations with three inputs: random numbers, ordered numbers and reverse ordered numbers with array size starting from 0 to 200. In quick sort worst and bast case is a little tricky to understand. It is still interesting to see how it performs in ordered and reveres order input. The average case is still the same as the random numbers performance.  

```c
#include <utility>

int partition( int* numbers, int beginning, int end ) {
    int pivot = numbers[end];
    int i = beginning - 1;
    for( int j = beginning; j < end; j++ ) {
        if( numbers[j] <= pivot ) {
            i++;
            std::swap( numbers[i], numbers[j] );
        }
    }
    std::swap( numbers[i + 1], numbers[end] );
    return i + 1;
}

void quickSort( int* numbers, int beginning, int end ) {
    if( beginning < end ) {
        int split = partition( numbers, beginning, end );
        quickSort( numbers, beginning, split - 1 );
        quickSort( numbers, split + 1, end );
    }
}

```

Quick sort is more tricky to understand. The `quckSort` function looks a lot like `mergeSort` function they both are recursive and split the array in separate parts, but the way they split and sort is very different.  
I cant explane it simple enough so if you want to understand it and the code is not enough go to the wikipedia page. It hase nice explanation with animations. 

Further reading: [Wikipedia Quick Sort](https://en.wikipedia.org/wiki/Quicksort).

---

#### Source:

* [Wikipedia](https://www.wikipedia.org)
* [Introduction to Algorithms by Thomas H. Cormen](https://en.wikipedia.org/wiki/Introduction_to_Algorithms)

