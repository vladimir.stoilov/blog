---
title: XOR and XOR Cipher
description: What is XOR and how to encrypt with it?
date: "2019-06-08"
---

## XOR

XOR (exclusive or) is a logical gate. In software developer is used for optimizing algorithms and encrypting data.

I will use $\oplus$ for XOR.

![](XOR_ANSI.svg)

The truth table:

| Input A | Input B | Output |
| :------ | :------ | :----- |
| 0       | 0       | 0      |
| 1       | 0       | 1      |
| 0       | 1       | 1      |
| 1       | 1       | 0      |

$$
A \oplus B = \overline{A} . B + A . \overline{B} = (\overline{A} + \overline{B}).(A + B)
$$

---

Once I saw an interview question and the best solution was with XOR. The problem was:

You have two arrays A and B. The `sizeof(A) = n`, `sizeof(B) = n - 1` and A contains all the elements that are inside B plus one more. You have to find which element is missing from B. For example:

`A = [ 5 , 54, 14, 8, 4, 97 ]`

`B = [ 54, 8, 97, 5, 4 ]`

The missing element is `14`.  Here is the solution in `C++` (`^` means XOR in `C++`) :

```c
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int getMissingElement( const vector<int>& A, const vector<int>& B )
{
    int number = 0;
    for( const int n : A )
       number ^= n;

    for( const int n : B )
       number ^= n;

    return number;
}

int main()
{
    vector<int> A = { 5, 54, 14, 8, 4, 97 };
    vector<int> B = { 54, 8, 97, 5, 4 };
    cout << "The missing element is: " << getMissingElement( A, B ) << endl;
    return 0;
}
```

### Output:

```
The missing element is: 14
```

The reason why this is working is because XOR is reversible.

$X \oplus Y = Z$  
$Z \oplus Y = X$  

For example:  
$"Fish" \oplus "abcd" = Z$:
$$
\begin{array}{r}
&01100001 \: 01100010 \: 01100011 \: 01100100_2\\
\oplus
&01000110 \: 01101001 \: 01110011 \: 01101000_2\\
\hline
&00100111 \: 00001011 \: 00010000 \: 00001100_2
\end{array}
$$

$Z \oplus "abcd" = "Fish"$:  
$$
\begin{array}{r}
&00100111 \: 00001011 \: 00010000 \: 00001100_2\\
\oplus
&01000110 \: 01101001 \: 01110011 \: 01101000_2\\
\hline
&01100001 \: 01100010 \: 01100011 \: 01100100_2
\end{array}
$$

We will use this quality of XOR to develop a simple but unbreakable еncryption algorithm.

## XOR Cipher

XOR encryption is used when you need security and speed at the same time.  

Here is basic implementation:

```c
void xorcyprt( std::string& message, const std::vector<unsigned char>& key ) {
    int keyIndex = 0;
    for( char& b : message ) {
        b ^= key[keyIndex++];
        keyIndex %= key.size();
    }
}

void encrypt( std::string& message, const std::vector<unsigned char>& key ) {
    xorcyprt( message, key );
}

void decrypt( std::string& message, const std::vector<unsigned char>& key ) {
    xorcyprt( message, key );
}

int main() {
   std::string message = "So Long, and Thanks for All the Fish.";
   // Generate sequence of random bytes.
   std::vector<unsigned char> key = generateRandomKey( message.length() );

   printBytes( "Original", message );

   encrypt( message, key );
   printBytes( "Encrypted", message );

   decrypt( message, key );
   printBytes( "Decrypted", message );
   return 0;
}
```

### Output:

```
Original:   536F204C6F6E672C20616E64205468616E6B7320666F7220416C6C2074686520466973682E
Encrypted:  573EF05DCD9E6B4B46601D0D62DC0641CFC900E22A9F0B89A926764C321C47CC33842A3F5E
Decrypted:  536F204C6F6E672C20616E64205468616E6B7320666F7220416C6C2074686520466973682E
```

This type of encryption is called [One-time pad](https://en.wikipedia.org/wiki/One-time_pad).

In theory the encrypted message is unbreakable without the `key` because the length of the `key` is the same with the message.  
The problem with this encryption is that if someone finds the key, he can decrypt everything. Or if the key is to short someone may guess the key from analyzing the encrypted data ([The Imitation Game](https://en.wikipedia.org/wiki/The_Imitation_Game)).  
You should transmit the key over a secure channel and the longer the key the better.  

So were should I use XOR encryption?  
First we should answer the question "Were you should **NOT** use it?" 
- When you don't have a secure channel to transmit the key.
- When there is sensitive information (It's better for the user to wait for couple of milliseconds then to have his information leaked )
- When performance those not matter alot use more secure encryption algorithm. (Saving encrypted files, sending request to server, ...)  

If non of the above are true you should consider using XOR encryption.  
For example you are making screen-sharing application and while transmitting the user screen over the network you don't want someone the get the packets and view his screen without permission. So you want to encrypt the packets but you don't want the performance to suffer because of that. In this case XOR encryption is a good choice. As long as you transmit the key over a secure channel and maybe generate new key every few minutes.

---

##### Source:

[Wikipedia XOR cipher](https://en.wikipedia.org/wiki/XOR_cipher)  
[Wikipedia XOR](https://en.wikipedia.org/wiki/Exclusive_or)

