# Parsing Lisp

`(+ 1 3 (- 10 34))`

`+ 1 3 ( - 10 34 )`

`- 10 34`


* ( -> start func
* ) -> end func
```
f: +
   1
   2
   f: -
      10
      34
```

spliting to elements shold split this  `+ 1 3 ( - 10 34 )`
to:
*	\+
*	1
*	3
*	( - 10 34 )

## How it works 
read until char != ' ', '\t', '\n' ...  
if char == '(' read until ')'  
else read until char != ' ', '\t', '\n' ...  